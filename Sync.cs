﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using System.IO;

namespace SyncPTGT2SW
{
    public class Sync
    {
        public static void Tirezone(string dateYesterdayString, string SFTPHost, string SFTPUsername, string SFTPPassword)
        {

            // Change file path here
            string workingDirectory = Environment.CurrentDirectory;
            var systemPath = Directory.GetParent(workingDirectory).Parent.Parent.FullName;

            var createFilePath = Path.Combine(systemPath,"TZ-"+ dateYesterdayString + ".csv");

            String server = "gtdbdev.database.windows.net";
            String databaseName = "TIREZONE_DEV";
            String username = "gajahdev";
            String password = "abcdef";

            String connectionString = "Data Source=" + server + ";User id=" + username + ";Password=" + password + ";Initial Catalog=" + databaseName;
            //String connectionString = "Server = localhost; Database = GT_SYNC; Integrated Security = True";



            /*String connectionString = "Data Source=" + server + ";Initial Catalog=" + databaseName + ";Trusted_Connection=true";*/
            String query = "select" +
                 "  oh.InvCode+(case 5-len(oh.invnumber) when 1 then cast('0' as char(1))+cast(oh.InvNumber as char(4))" +
                                                       " when 2 then cast('00' as char(2))+cast(oh.InvNumber as char(3))" +
                                                     " when 3 then cast('000' as char(3))+cast(oh.InvNumber as char(2))" +
                                                     " when 4 then cast('0000' as char(4))+cast(oh.InvNumber as char(1))" +
                             " else cast(oh.InvNumber as char(5))" +
                             " end) as OrderNumber," +
                 " mc.CustomerMobile," +
                 " oh.OrderSource," +
                 " day(oh.TransOutDate) as Date," +
                 " month(oh.TransOutDate) as Month," +
                 " year(oh.TransOutDate) as Year," +
                 " r.RetailId," +
                 " (select top 1 Region from GT_RAP_EMAIL_GTTZ where RetailId = r.RetailId and Status = 1) as Region," +
                 " oh.RetailCode," +
                 " r.RetailName," +
                 " r.RetailCity," +
                 " r.RetailProvince," +
                 " od.TireId," +
                 " p.PatternDesc," +
                 " t.TireCode," +
                 " t.TireSize," +
                 " (case when charindex('/',t.TireSize)>3 then substring(t.TireSize, charindex('/',t.TireSize)-3, 3) else null end) as Width," +
                 " t.AspectRatio," +
                 " t.RimSize," +
                 " od.QtyOut," +
                 " od.UnitPrice," +
                 " ( select top 1 RetailClass" +
                     " from GT_RAP_RETAIL_CLASS c" +
                     " where c.RetailCode = r.RetailCode" +
                      " and '01-DEC-2020' between" +
                      " convert(datetime, '1-'+c.StartPeriode)" +
                      " and (case when c.EndPeriode is not null then convert(datetime, '1-'+ c.EndPeriode) else getdate() end)) as Class" +
                 " from GT_RAP_TRANSACT_OUT_D od" +
                " join GT_RAP_TRANSACT_OUT_H oh on od.TransOutHId = oh.TransOutHId" +
                " join GT_RAP_RETAIL r on oh.RetailCode = r.RetailCode" +
                " join GT_RAP_MASTER_TIRE t on od.TireId = t.TireId" +
                " join GT_RAP_PATTERN p on t.PatternId = p.PatternId" +
                " join GT_RAP_MASTER_CARCODE mc on oh.CarCodeId = mc.CarCodeId and oh.RetailCode = mc.RetailCode" +
                " where MONTH( oh.TransOutDate) = 11 AND  yEAR( oh.TransOutDate) = 2020 and oh.note not like 'Sync Penjualan%' " +
                " and p.RetailCode = 'RETAIL'" +
                " and p.CatId = (case when '1' is not null then '1' else p.CatId end)" +
                " order by oh.RetailCode, oh.TransOutDate;";

            /*String query = "Select * from test_data";*/
            //" where oh.TransOutDate = " + dateYesterdayString +
            try
            {
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionString;
                Console.WriteLine("Connected to db " + databaseName);
                var csv = new StringBuilder();

                connection.Open();                
                SqlCommand command = new SqlCommand(query, connection);
                command.CommandTimeout = 100;
                csv.AppendLine("OrderNumber,CustomerMobile,OrderSource,Date,Month,Year,Region,RetailCode,RetailName,RetailCity,RetailProvince,PatternDesc,TireCode,TireSize,Width,AspectRatio,RimSize,QtyOut,UnitPrice,Class");

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var newline = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19}",
                            // {0} to {5}
                            reader["OrderNumber"], reader["CustomerMobile"], reader["OrderSource"], reader["Date"], reader["Month"], reader["Year"],
                            // {6} to {11}
                            reader["Region"], reader["RetailCode"], reader["RetailName"], reader["RetailCity"], reader["RetailProvince"], reader["PatternDesc"],
                            // {12} to {17}
                            reader["TireCode"], reader["TireSize"], reader["Width"], reader["AspectRatio"], reader["RimSize"], reader["QtyOut"],
                            // {18} to {19}
                            reader["UnitPrice"], reader["Class"]);
                        csv.AppendLine(newline);

                    }
                    //Console.WriteLine("Creating file");

                    //Console.WriteLine("Appending text");
                    File.WriteAllText(createFilePath, csv.ToString());
                    //Console.WriteLine("Appended text");
                }
                connection.Close();
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }


            
            string workingdirectory = "/SYNC_POSS_PTGT2SW/Penjualan";

            using (var client = new SftpClient(SFTPHost, SFTPUsername, SFTPPassword))
            {
                try
                {
                    //Console.WriteLine("Client connected");
                    client.Connect();
                    Console.WriteLine("SFTP Connected");
                    if (client.IsConnected)
                    {
                        
                        client.ChangeDirectory(workingdirectory);

                        using (var fileStream = new FileStream(createFilePath, FileMode.Open))
                        {

                            Console.WriteLine("SFTP working directory = " + client.WorkingDirectory);
                            client.BufferSize = 4 * 1024;
                            Console.WriteLine("Uploading File");
                            client.UploadFile(fileStream, Path.GetFileName(createFilePath));
                            fileStream.Close();
                            File.Delete(createFilePath);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

           

        }

        public static void MotoXpress(string dateYesterdayString, string SFTPHost, string SFTPUsername, string SFTPPassword)
        {
            // Change file path here
            string workingDirectory = Environment.CurrentDirectory;
            var systemPath = Directory.GetParent(workingDirectory).Parent.Parent.FullName;

            var createFilePath = Path.Combine(systemPath, "MX-" + dateYesterdayString + ".csv");

            String server = "gtdbdev.database.windows.net";
            String databaseName = "TIREZONE_DEV";
            String username = "gajahdev";
            String password = "abcdef";

            String connectionString = "Data Source=" + server + ";User id=" + username + ";Password=" + password + ";Initial Catalog=" + databaseName;
            //String connectionString = "Server = localhost; Database = GT_SYNC; Integrated Security = True";



            /*String connectionString = "Data Source=" + server + ";Initial Catalog=" + databaseName + ";Trusted_Connection=true";*/
            String query = "select" +
                 "  oh.InvCode+(case 5-len(oh.invnumber) when 1 then cast('0' as char(1))+cast(oh.InvNumber as char(4))" +
                                                       " when 2 then cast('00' as char(2))+cast(oh.InvNumber as char(3))" +
                                                     " when 3 then cast('000' as char(3))+cast(oh.InvNumber as char(2))" +
                                                     " when 4 then cast('0000' as char(4))+cast(oh.InvNumber as char(1))" +
                             " else cast(oh.InvNumber as char(5))" +
                             " end) as OrderNumber," +
                 " mc.CustomerMobile," +
                 " oh.OrderSource," +
                 " day(oh.TransOutDate) as Date," +
                 " month(oh.TransOutDate) as Month," +
                 " year(oh.TransOutDate) as Year," +
                 " r.RetailId," +
                 " (select top 1 Region from GT_RAP_EMAIL_GTTZ where RetailId = r.RetailId and Status = 1) as Region," +
                 " oh.RetailCode," +
                 " r.RetailName," +
                 " r.RetailCity," +
                 " r.RetailProvince," +
                 " od.TireId," +
                 " p.PatternDesc," +
                 " t.TireCode," +
                 " t.TireSize," +
                 " (case when charindex('/',t.TireSize)>3 then substring(t.TireSize, charindex('/',t.TireSize)-3, 3) else null end) as Width," +
                 " t.AspectRatio," +
                 " t.RimSize," +
                 " od.QtyOut," +
                 " od.UnitPrice," +
                 " ( select top 1 RetailClass" +
                     " from GT_RAP_RETAIL_CLASS c" +
                     " where c.RetailCode = r.RetailCode" +
                      " and '01-DEC-2020' between" +
                      " convert(datetime, '1-'+c.StartPeriode)" +
                      " and (case when c.EndPeriode is not null then convert(datetime, '1-'+ c.EndPeriode) else getdate() end)) as Class" +
                 " from GT_RAP_TRANSACT_OUT_D od" +
                " join GT_RAP_TRANSACT_OUT_H oh on od.TransOutHId = oh.TransOutHId" +
                " join GT_RAP_RETAIL r on oh.RetailCode = r.RetailCode" +
                " join GT_RAP_MASTER_TIRE t on od.TireId = t.TireId" +
                " join GT_RAP_PATTERN p on t.PatternId = p.PatternId" +
                " join GT_RAP_MASTER_CARCODE mc on oh.CarCodeId = mc.CarCodeId and oh.RetailCode = mc.RetailCode" +
                " where MONTH( oh.TransOutDate) = 11 AND  yEAR( oh.TransOutDate) = 2020 and oh.note not like 'Sync Penjualan%' " +
                " and p.RetailCode = 'RETAIL'" +
                " and p.CatId = (case when '1' is not null then '1' else p.CatId end)" +
                " order by oh.RetailCode, oh.TransOutDate;";

            /*String query = "Select * from test_data";*/
            //" where oh.TransOutDate = " + dateYesterdayString +
            try
            {
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionString;
                Console.WriteLine("Connected to db " + databaseName);
                var csv = new StringBuilder();

                connection.Open();
                //Console.WriteLine("db open");
                SqlCommand command = new SqlCommand(query, connection);
                //Console.WriteLine("command");
                csv.AppendLine("OrderNumber,CustomerMobile,OrderSource,Date,Month,Year,Region,RetailCode,RetailName,RetailCity,RetailProvince,PatternDesc,TireCode,TireSize,Width,AspectRatio,RimSize,QtyOut,UnitPrice,Class");

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var newline = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19}",
                            // {0} to {5}
                            reader["OrderNumber"], reader["CustomerMobile"], reader["OrderSource"], reader["Date"], reader["Month"], reader["Year"],
                            // {6} to {11}
                            reader["Region"], reader["RetailCode"], reader["RetailName"], reader["RetailCity"], reader["RetailProvince"], reader["PatternDesc"],
                            // {12} to {17}
                            reader["TireCode"], reader["TireSize"], reader["Width"], reader["AspectRatio"], reader["RimSize"], reader["QtyOut"],
                            // {18} to {19}
                            reader["UnitPrice"], reader["Class"]);
                        csv.AppendLine(newline);

                    }
                    //Console.WriteLine("Creating file");

                    //Console.WriteLine("Appending text");
                    File.WriteAllText(createFilePath, csv.ToString());
                    //Console.WriteLine("Appended text");
                }
                connection.Close();
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }



            string workingdirectory = "/SYNC_POSS_PTGT2SW/Penjualan";

            using (var client = new SftpClient(SFTPHost, SFTPUsername, SFTPPassword))
            {
                try
                {
                    //Console.WriteLine("Client connected");
                    client.Connect();
                    Console.WriteLine("SFTP Connected");
                    if (client.IsConnected)
                    {
                        //Console.WriteLine("Client connected");
                        client.ChangeDirectory(workingdirectory);

                        using (var fileStream = new FileStream(createFilePath, FileMode.Open))
                        {

                            Console.WriteLine("SFTP working directory = " + client.WorkingDirectory);
                            client.BufferSize = 4 * 1024;
                            Console.WriteLine("Uploading File");
                            client.UploadFile(fileStream, Path.GetFileName(createFilePath));
                            fileStream.Close();
                            File.Delete(createFilePath);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

           


        }
    }
}
